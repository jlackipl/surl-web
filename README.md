# SURL Web - Short URL Web

<div align="center" width="100%">
    <img src="./logo.png"/>
</div>

## Project setup
Install npm on your local machine.

Run SURL Api on your local machine. https://gitlab.com/jlackipl/surl

Web application assume that SURL api will be under: `http://localhost:8000`
if you run API under different address then copy `.env` to `.env.local` and change `VUE_APP_SURL_API_HOST` to correct value.

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
